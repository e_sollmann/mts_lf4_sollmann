package bowling.classes;

public class Game {

	private String Player;
	private Throw[] Throws;
	
	public Game(String player)
	{
		Player = player;		
		Throws = new Throw[10];
		
		for(int t = 0; t < Throws.length; t++)
		{
			Throws[t] = null;
		}
	}
	
	public int CalculateScore()
	{
		int total = 0;
		
		for(Throw th : Throws)
		{
			total += th.GetTotalScore();
		}
		
		return total;
	}
	
	public void AddThrow(Throw thr)
	{
		for(int t = 0; t < Throws.length; t++)
		{
			if(Throws[t] == null)
			{
				Throws[t] = thr;
				return;
			}
		}
	}
	
	public Throw[] GetThrows()
	{
		return Throws;
	}

	public String GetPlayer()
	{
		return Player;
	}
}
