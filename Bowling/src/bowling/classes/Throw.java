package bowling.classes;

public class Throw {

	private boolean LastThrow;
	private int[] Score;
	
	public Throw(boolean lastThrow, int[] score)
	{
		Score = score;
		LastThrow = lastThrow;
	}
	
	public int GetTotalScore()
	{
		return Score[0] + Score[1] + Score[2];
	}
	
	public int GetScore(int index)
	{
		return Score[index];
	}
	
	public boolean IsLastThrow()
	{
		return LastThrow;
	}
	
	public static boolean IsMiss(Throw th, int scoreIndex)
	{
		if(th.GetScore(scoreIndex) == 0)
		{
			return true;
		}
		
		return false;
	}
	
	public static boolean IsSpare(Throw th, int scoreIndex)
	{
		if(scoreIndex == 1)
		{
			if(th.GetScore(0) + th.GetScore(1) == 10)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean IsStrike(Throw th, int scoreIndex)
	{
		if(th.GetScore(scoreIndex) == 10)
		{
			return true;
		}
		
		return false;
	}
}
