package bowling.classes;

import java.util.Scanner;

public class Core {

	/*
	 * 
	 * Ich entschuldige mich f�r mein f�r Java eher un�bliches Codelayout 
	 * aber ich finde die Konventionen von C# einfach sch�ner und sollte
	 * diese auch beibehalten. :D
	 * 
	 * Ich bitte um Ihr Verst�ndnis.
	 * 
	 * mfg Erik Sollmann
	 * 
	 * */
	
	public static void main(String[] args)
	{
		PlayGame();
	}
	
	private static void PlayGame()
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Insert Player Name: ");
		String playerName = sc.next();
		System.out.println("Welcome " + playerName + "\n");
		
		Game game = new Game(playerName);
		
		for(int i = 0; i < 9; i++)
		{		
			int remaining = 10;
			int[] score = new int[] {0, 0};
			
			for(int t = 0; t < 2; t++)
			{
				sc.nextLine();
				
				int thrown = Utils.RandomInteger(0, remaining);
				remaining -= thrown;
				
				score[t] = thrown;
				
				System.out.println("Round " + (i + 1) + "/10 : Throw " + (t + 1) + ": " + score[t] + " thrown, " + remaining + " remaining");
				
				if(t == 0)
				{
					if(score[t] == 10)
					{
						remaining = 10;
					}
				}
			}
			
			game.AddThrow(new Throw(false, new int[] {score[0], score[1], 0}));
		
			PrintGame(game);
		}
		
		int remaining = 10;
		int[] score = new int[] {0, 0, 0};
		
		for(int t = 0; t <= 2; t++)
		{
			sc.nextLine();
			
			int thrown = Utils.RandomInteger(0, remaining);
			remaining -= thrown;
			
			score[t] = thrown;
			
			System.out.println("Round 10/10 : Throw " + (t + 1) + ": " + score[t] + " thrown, " + remaining + " remaining");
			
			if(t == 1)
			{
				if(!(score[0] + score[1] == 10 || score[1] == 10))
				{
					break;
				}
				else if(score[0] + score[1] == 10 || score[1] == 10)
				{
					remaining = 10;
				}
			}
		}
		
		game.AddThrow(new Throw(true, new int[] {score[0], score[1], score[2]}));
		
		PrintGame(game);
		
		sc.close();
		
		System.out.println("\n\n === GAME OVER ===\n");
		
		System.out.println("Total Score: " + game.CalculateScore());
	}
	
	private static void PrintGame(Game game)
	{
		System.out.println();
		
		for(int t = 0; t < 10; t++)
		{
			System.out.print("Throw No. " + (t + 1) + ": ");
			
			Throw th = game.GetThrows()[t];
			
			if(th != null)
			{
				String[] score = new String[] {Integer.toString(th.GetScore(0)), Integer.toString(th.GetScore(1)), Integer.toString(th.GetScore(2))};
				
				for(int s = 0; s <= 2; s++)
				{
					if(Throw.IsMiss(th, s))
					{
						score[s] = "-";
					}
					else if(Throw.IsSpare(th, s))
					{
						score[s] = "/";
					}
					else if(Throw.IsStrike(th, s))
					{
						score[s] = "X";
					}
				}
				
				System.out.print(" " + score[0] + " | " + score[1]);
				
				if(th.IsLastThrow())
				{
					System.out.print(" | " + score[2]);					
				}
			}
			System.out.println();
		}
		System.out.println();
	}
	
}
