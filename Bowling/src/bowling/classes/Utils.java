package bowling.classes;

import java.util.Random;

public class Utils {
	
	public static int RandomInteger(int min, int max)
	{
		if(min < max)
		{
			Random rnd = new Random();
			return rnd.nextInt((max - min) + 1) + min;
		}
		
		return 0;
	}
	
}
