import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import javafx.scene.transform.Transform;

public class Airplane {

	private String Registration;
	private String Type;
	private String FlightNumber;
	private Vec3 Position;
	private Vec3 Direction;
	private BufferedImage Image;
	
	private AffineTransform Transform;
	
	private boolean Big;
	
	private Spielfeld ParentWindow;
	
	public Airplane(Spielfeld parentWindow, String registration, String type, String flightnumber, Vec3 position, Vec3 direction, boolean big)
	{
		this.Registration = registration;
		this.Type = type;
		this.FlightNumber = flightnumber;
		this.Position = position;
		this.Big = big;
		
		this.ParentWindow = parentWindow;
		
		if(big)
		{
			try {
				Image = ImageIO.read(this.getClass().getResourceAsStream("airplane_big.png"));
			} catch (IOException e) {}
		}
		else
		{
			try {
				Image = ImageIO.read(this.getClass().getResourceAsStream("airplane_small.png"));
			} catch (IOException e) {}
		}
		
		Transform = new AffineTransform();
		
		setDirection(direction);
	}
	
	public void fly()
	{
		this.Position.setX(this.Position.getX() + this.Direction.getX());
		this.Position.setY(this.Position.getY() + this.Direction.getY());
		this.Position.setZ(this.Position.getZ() + this.Direction.getZ());
		
		if(this.Position.getX() < -Image.getWidth())
		{
			this.Position.setX(this.ParentWindow.getWidth() + Image.getWidth());
		}
		else if(this.Position.getX() > this.ParentWindow.getWidth() + Image.getWidth())
		{
			this.Position.setX(-Image.getWidth());
		}
		
		if(this.Position.getY() < -Image.getHeight())
		{
			this.Position.setY(this.ParentWindow.getHeight() + Image.getHeight());
		}
		else if(this.Position.getY() > this.ParentWindow.getHeight() + Image.getHeight())
		{
			this.Position.setY(-Image.getHeight());
		}
	}
	
	public void setDirection(Vec3 direction)
	{
		this.Direction = direction;
	}
	
	public void SetSpeed(double speed)
	{
		this.Direction.setX(this.Direction.getX() * speed);
		this.Direction.setY(this.Direction.getY() * speed);
		this.Direction.setZ(this.Direction.getZ() * speed);
	}
	
	public void draw(Graphics g)
	{
		double x = this.Direction.getX();
		double y = this.Direction.getY();
		double angle = Math.atan2(y, x);
		
		if(!this.Big)
		{
			angle += Math.PI;
		}
		
		Transform.setToTranslation(this.Position.getX(), this.Position.getY());
		
		Transform.rotate(angle);
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2d.drawImage(Image, Transform, null);
	}
}
