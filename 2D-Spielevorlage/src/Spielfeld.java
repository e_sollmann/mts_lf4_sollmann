
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;

public class Spielfeld extends JPanel{
	
	private List<Airplane> Planes = new ArrayList<Airplane>();
	
	public Spielfeld(){}
	
	/**
	 * Spielfl�che initialisieren
	 */
	public void init(){
		GeneratePlanes(20);
	}
	
	@Override
	public void paintComponent(Graphics g){
		
		//Spielfeld zeichnen
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		for(Airplane plane : Planes)
		{
			plane.fly();
			plane.draw(g);
		}
	}
	
	public void GeneratePlanes(int amount)
	{
		Random rnd = new Random();
		
		for(int i = 0; i < amount; i++)
		{
			String name = "Airplane " + Integer.toString(i);
			String type = "Airbus A380";
			String flightNo = Integer.toString(rnd.nextInt(10000));
			int posX = rnd.nextInt(this.getWidth());
			int posY = rnd.nextInt(this.getHeight());
			Vec3 pos = new Vec3(posX, posY, 0);
			Vec3 dir = new Vec3(rnd.nextDouble() - 0.5, rnd.nextDouble() - 0.5, rnd.nextDouble() - 0.5);
			boolean big = rnd.nextBoolean();
			
			Airplane plane = new Airplane(this, name, type, flightNo, pos, dir, big);
			plane.SetSpeed(rnd.nextInt(6) + 3);
			
			Planes.add(plane);
		}
		
	}
}
